package conduit.ui.config;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;

public final class AppiumServerController {

    private static AppiumServiceBuilder builder;
    private static AppiumDriverLocalService service;

    public static void startServer(){
		DesiredCapabilities capabilities = new DesiredCapabilities();
        System.out.println("Building and starting the server:");
		capabilities.setCapability("nativeWebScreenshot", true);
		//capabilities.setCapability("unlockType", "");
		//capabilities.setCapability("unlockKey", "");
        builder = new AppiumServiceBuilder();
        builder.withIPAddress("10.0.2.15");
        builder.usingPort(4723);
        builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
        builder.withArgument(GeneralServerFlag.LOG_LEVEL, "info");
        builder.withArgument(GeneralServerFlag.BASEPATH, "/wd/hub/");
        builder.withArgument(() -> "--allow-insecure","chromedriver_autodownload");
		builder.withCapabilities(capabilities);
        service = AppiumDriverLocalService.buildService(builder);
        service.start();
        System.out.println("Server started on Port - " + 4723);
    }
    
    public static void stopServer() {
        try {
            System.out.println("Trying to stop the server...");
            service.stop();
            System.out.println("Success, Server stopped.");
        } catch (Exception e) {
            System.out.println("Appium server could not be stopped.");
        }
    }
    
}