package conduit.ui.glue.common;

import static conduit.ui.pages.HomePage.LINK_SIGN_IN;
import static conduit.ui.pages.SignInPage.TEXT_TITLE;

import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.ensure.Ensure;

public class SignInDefinitions {
    
    @Quando("o usuário {ator} abre a tela de login")
    public void quandoAbroATelaLogin(Actor actor){
        actor.attemptsTo(Click.on(LINK_SIGN_IN));
    }

    @Então("a tela deve exibir o título {string}")
    public void entaoATelaExibeOTitulo(String texto) {
        Ensure.that(TEXT_TITLE).text().isEqualTo(texto);
    }

}
