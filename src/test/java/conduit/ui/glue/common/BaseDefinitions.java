package conduit.ui.glue.common;

import conduit.ui.config.MultiDeviceCast;
import conduit.ui.pages.HomePage;
import io.cucumber.java.Before;
import io.cucumber.java.ParameterType;
import io.cucumber.java.pt.Dado;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;

public class BaseDefinitions {
    
    @ParameterType("(desktop|mobile)")
    public Actor ator(String actorName) {
        return OnStage.theActorCalled(actorName);
    }

    @Before
    public void setTheStage() {
        OnStage.setTheStage(new MultiDeviceCast());
    }

    @Dado("que o usuário {ator} acessou a Home Page")
    public void dadoQueAcesseiAHomePage(Actor actor) {
        actor.wasAbleTo(Open.browserOn().the(HomePage.class));
    }

}
