package conduit.ui.glue.mobile;

import conduit.ui.config.AppiumServerController;
import io.cucumber.java.AfterAll;
import io.cucumber.java.BeforeAll;

public class MobileDefinitions {
    
    @BeforeAll
    public static void beforeAll() {
        AppiumServerController.startServer();
    }

    @AfterAll
    public static void afterAll() {
        AppiumServerController.stopServer();
    }    
}
