package conduit.ui.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://127.0.0.1/#/")
public class HomePage extends PageObject {

    public static Target LINK_SIGN_IN = Target.the("Sign in Link").located(By.linkText("Sign in"));    
    
}
