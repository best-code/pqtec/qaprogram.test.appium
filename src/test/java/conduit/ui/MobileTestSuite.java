package conduit.ui;

import org.junit.runner.RunWith;
import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        glue = {"conduit.ui.glue.common","conduit.ui.glue.mobile"},
        features = "src/test/resources/features/mobile"
)
public class MobileTestSuite {}
