#language: pt

@MOBILE
Funcionalidade: Login

Cenário: Usuário deve acessar a tela de login com sucesso
    Dado que o usuário mobile acessou a Home Page
    Quando o usuário mobile abre a tela de login
    Então a tela deve exibir o título "Sign In"