#language: pt

@DESKTOP
Funcionalidade: Login

Cenário: Usuário deve acessar a tela de login com sucesso
    Dado que o usuário desktop acessou a Home Page
    Quando o usuário desktop abre a tela de login
    Então a tela deve exibir o título "Sign In"