# Rodar os testes mobile no Android:

1. Ativar as opções do desenvolvedor no smartphone
   https://canaltech.com.br/android/como-ativar-modo-desenvolvedor-android/
2. Na tela do desenvolvedor, habilitar as opções "Permanecer ativo" e "Depuração USB"
    >***OBS***: Caso o smartphone seja da empresa Xiaomi, ira ser necessario a ativação dos seguintes icones:
    >- "Depuração USB"
    >- "Instalar via USB"
    >- "Depuração USB (Config. de segurança)"

![imagem](./image/photo_2022-04-26_14-23-45.jpg)

3. Instalar as extensões do Virtual Box na máquina hospedeira
   https://download.virtualbox.org/virtualbox/6.1.32/Oracle_VM_VirtualBox_Extension_Pack-6.1.32.vbox-extpack
4. Conectar o smartphone no usb da máquina hospedeira da VM
5. No Virtual Box, com a VM iniciada, acessar Configurações->USB->Filtro de Dispositivo USB

![imagem](./image/Screenshot_2.png)

6. Clicar no botão com ícone do USB e símbolo "+" e adicionar a opção que tenha o nome do dispositivo conectado
7. No terminal da VM, executar o comando ```adb devices``` e verificar se uma tela para aceite da permissão do dispositivo vai aparecer no smartphone
   >**OBS**: Caso não apareça, alterar o modo de conexão usb do smartphone (geralmente funciona com Transferência de arquivos ou tranferência de fotos)
8. Quando a tela de permissão aparecer no smartphone, aceitar (pode ser marcada a opção para sempre confiar no dispositivo)
9. Executar o comando ```adb devices``` novamente e copiar o identificador do dispositivo que aparecer (vale a mesma observação do passo 7)
10. Faça o clone do git: ```git clone https://gitlab.com/best-code/pqtec/qaprogram.test.appium.git```
11. Abrir o arquivo /home/qa/qaprogram.test.appium/src/test/resources/serenity.conf
12. No atributo 'deviceName' do bloco appium, alterar o valor para o nome que foi retornado pelo comando adb devices
13. No atributo 'platformVersion' do bloco appium, alterar o valor para a versão do Android do dispositivo conectado
14. Salvar as alterações no arquivo serenity.conf
15. No terminal da VM, mapear as portas da aplicação para o dispositivo móvel
    - adb reverse tcp:4200 tcp:4200
    - adb reverse tcp:3333 tcp:3333
16. Na pasta do projeto de automação, executar o teste com o comando mvn clean verify -Dtest.suite="MobileTestSuite"
    > ***OBS***: Enquanto estiver executando o Maven deve ser acompanhado a tela do celular para dar permissão das instalações do Appium
17. No final da execução, no "Explorador" no menu lateral do VSCode click com o botão direito do Mouse na pasta ```target/site/serenity```para baixar a pasta.
18. No fim do download da pasta "serenity", execute o arquivo "index.html" 

![imagem](./image/Screenshot_1.png)

>***OBS***: Os comandos 'adb devices' e 'adb reverse' deverão ser executados toda vez que os testes forem rodar em um novo terminal